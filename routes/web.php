<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('layouts.backend');
});

Route::prefix('backend')->middleware(['auth','admin'])->namespace('Backend')->group(function (){

    Route::get('category/create', 'CategoryController@create')->name('category.create');
    Route::get('category', 'CategoryController@index')->name('category.index');
    Route::post('category', 'CategoryController@store')->name('category.store');
    Route::get('category/{id}', 'CategoryController@show')->name('category.show');
    Route::get('category/{id}/edit', 'CategoryController@edit')->name('category.edit');
    Route::put('category/{id}', 'CategoryController@update')->name('category.update');
    Route::delete('category/{id}', 'CategoryController@destroy')->name('category.destroy');

    Route::get('product/create', 'ProductController@create')->name('product.create');
    Route::get('product', 'ProductController@index')->name('product.index');
    Route::post('product', 'ProductController@store')->name('product.store');
    Route::get('product/{id}', 'ProductController@show')->name('product.show');
    Route::get('product/{id}/edit', 'ProductController@edit')->name('product.edit');
    Route::put('product/{id}', 'ProductController@update')->name('product.update');
    Route::delete('product/{id}', 'ProductController@destroy')->name('product.destroy');

    Route::get('module','ModuleController@index')->name('module.index');
    Route::get('module/create','ModuleController@create')->name('module.create');
    Route::post('module','ModuleController@store')->name('module.store');
    Route::get('module/{id}','ModuleController@show')->name('module.show');
    Route::get('module/{id}/edit','ModuleController@edit')->name('module.edit');
    Route::put('module/{id}','ModuleController@update')->name('module.update');
    Route::delete('module/{id}', 'ModuleController@destroy')->name('module.destroy');

    Route::get('permission','PermissionController@index')->name('permission.index');
    Route::get('permission/create','PermissionController@create')->name('permission.create');
    Route::post('permission','PermissionController@store')->name('permission.store');
    Route::get('permission/{id}','PermissionController@show')->name('permission.show');
    Route::get('permission/{id}/edit','PermissionController@edit')->name('permission.edit');
    Route::put('permission/{id}','PermissionController@update')->name('permission.update');
    Route::delete('permission/{id}', 'PermissionController@destroy')->name('permission.destroy');

    Route::get('role','RoleController@index')->name('role.index');
    Route::get('role/create','RoleController@create')->name('role.create');
    Route::post('role','RoleController@store')->name('role.store');
    Route::get('role/{id}','RoleController@show')->name('role.show');
    Route::get('role/{id}/edit','RoleController@edit')->name('role.edit');
    Route::put('role/{id}','RoleController@update')->name('role.update');
    Route::delete('role/{id}', 'RoleController@destroy')->name('role.destroy');

    Route::get('role/assignpermission/{id}', 'RoleController@assignPermission')->name('role.assignpermission');
    Route::post('role/savepermission/{id}', 'RoleController@savePermission')->name('role.savepermission');
});


\Illuminate\Support\Facades\Auth::routes();

Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
