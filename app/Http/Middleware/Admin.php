<?php

namespace App\Http\Middleware;
use App\Model\Role;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //check permission for other user
        $success = $this->checkUserRolePermission();
        if($success){
            //allow request
            return $next($request);
        }else{
            //disallow request
            return redirect()->route('home');
        }
    }

    function checkUserRolePermission(){
        $current_route = Route::current()->getName();
        $role_permissions_routes =  Role::find(Auth::user()->role_id)->permissions->pluck('route')->toArray();
        $is_authorized = in_array($current_route,$role_permissions_routes);
        return $is_authorized;
    }

}

