<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\ProductRequest;
use App\Model\Category;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['products'] = Product::all();
        return view('backend.product.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = Category::pluck('name', 'id');
        return view('backend.product.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {

        $request->request->add(['created_by' => Auth::user()->id]);


        if (!empty($request->has('photo'))) {
            $product_image = $request->file('photo');

            $image_name = uniqid() . '.' . $product_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/product');
            $product_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
        }

        $product = Product::create($request->all());

        if ($product) {
            $request->session()->flash('success_message', 'Product created successfully');
            return redirect()->route('product.index');
        }else{
            $request->session()->flash('error_message', 'Product creation failed');
            return redirect()->route('product.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['product']= Product::find($id);
        return view('backend.product.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['categories'] = Category::pluck('name', 'id');
        $data['product']= Product::find($id);
        return view('backend.product.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $request->request->add(['updated_by'=>Auth::user()->id]);
        $category= Product::find($id);
        if ($category->update($request->all())) {
            $request->session()->flash('success_message', 'Product Updated Successfully');
        }else{
            $request->session()->flash('error_message','Product updated Failed');
        }
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $category= Product::find($id);
        if ($category->delete()) {
            $request->session()->flash('success_message', 'Product Deleted Successfully');

        }else{
            $request->session()->flash('error_message','Product Deleted Failed');
        }
        return redirect()->route('product.index');
    }
}
