<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191|string|unique:products'.(request()->method()=="POST"?'':',name,'.$this->id),
            'slug' => 'required|max:191|string|unique:products'.(request()->method()=="POST"?'':',slug,'.$this->id),
            'price' => 'required|integer',
            'quantity' => 'required|integer',
            'stock' => 'required|integer',
            'short_description' => 'string|max:191',
            'description' => 'string|max:191',
            'photo'=>'required|max:1500',
        ];
    }
    function messages()
    {
        return[
            'name.unique'=>'Name must be unique',
            'name.required'=>'Name field is required',
            'name.string'=>'Name field must be of String type',

            'slug.unique'=>'Slug must be unique',
            'slug.required'=>'Slug field is required',
            'slug.string'=>'Slug field must be of String type',

            'price.required'=>'Price field is required',
            'price.integer'=>'Price field must be of integer type',

            'quantity.required'=>'Quantity field is required',
            'quantity.integer'=>'Quantity field must be of integer type',

            'stock.required'=>'Stock field is required',
            'stock.integer'=>'Stock field must be of integer type',

            'short_description.string'=>'Short Description must be of string type',
            'description.string'=>'Description must be of string type',

            'photo.required'=>'Image field is required',
            'photo.file'=>'Image file must be file type',
        ];
    }
}
