<?php

namespace App\Model;

use App\Model\Attribute;
use App\Model\Category;
use App\Model\Image;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table='products';
    protected $fillable=['category_id','name','slug','price','image','discount','quantity','stock','short_description','description',
        'feature_key','discount_key','meta_keyword','meta_description','status','created_by','updated_by'];

    public function category()
    {
        return $this->belongsTo(Category:: class);
    }

    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }
}


