<?php

namespace App\Model;

use App\Model\Permission;
use App\Model\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table='roles';
    protected $fillable=['name','status','created_by','updated_by'];

    function permissions(){
        return $this->belongsToMany(\App\Model\Permission::class);
    }
    function users() {
        return $this->hasMany(\App\Model\User::class);
    }
}
