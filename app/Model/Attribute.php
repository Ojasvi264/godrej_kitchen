<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;
    protected $table = 'attributes';
    protected $fillable =['name', 'product_id', 'value','status'];

    function product(){
    $this->belongsTo(Product::class);
    }
}
