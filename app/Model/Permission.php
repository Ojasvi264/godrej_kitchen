<?php

namespace App\Model;

use App\Model\Module;
use App\Model\Role;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;
    protected $table='permissions';
    protected $fillable=['module_id','name','route','status','created_by','updated_by'];

    function roles(){
        return $this->belongsToMany(Role::class);
    }

    function module(){
        return $this->belongsTo(Module::class);
    }
}
