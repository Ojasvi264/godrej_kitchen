<?php

namespace App\Model;

use App\Model\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $table ='categories';
    Protected $fillable=['name','slug','rank','status','meta_keyword','meta_description','created_by','updated_by'];

    function products(){
        return $this->hasMany(Product::class);
    }
}
