@extends('home')
@section('title', 'Product index page')

@section('content')
    <section class="content-header">
        <h1>
            Product Management
            <a href="{{route('product.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="{{route('home')}}"> Home</a></li>
            <li style="padding-right: 10px"><a href="{{route('product.index')}}">Category</a></li>
            <li>Index page</li>
        </ol>
    </section>
    <section class="content">

        <!-- Default box -->
        <div class="box">

            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Category Name</th>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>price</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($data['products'] as $product)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$product->category->name}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->slug}}</td>
                            <td>{{$product->price}}</td>
                            <td>
                                <div class="img-container">
                                    <a href="{{asset('images/product/' .$product->image)}}" data-lightbox="image-1" data-title="{{$product->image}}">
                                        <img src="{{asset('images/product/' .$product->image)}}" alt="" height="100" width="100">
                                    </a>
                                </div>
                            </td>
                            <td>
                                @if($product->status == 1)
                                    <span style="color: #1cc88a ">Active</span>
                                @else
                                    <span style="color: red">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('product.show',$product->id)}}" class="btn btn-info"> <i class="fa fa-eye"></i>View</a>
                                <a  href="{{route('product.edit',$product->id)}}" class="btn btn-warning"> <i class="fa fa-pencil"></i>Edit</a>

                                <form style="display: inline-block" action="{{route('product.destroy',$product->id)}}" method="post" onsubmit="return confirm('Are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box -->

    </section>
@endsection

