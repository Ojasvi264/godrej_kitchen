<div class="form-group">
    {!!  Form::label('category_id', 'Category Name'); !!}
    {!! Form::select('category_id', $data['categories'],null,['class' => 'form-control','placeholder' => 'Please Select Module']); !!}
</div>
<div class="form-group">
    {!!  Form::label('name', 'Name'); !!}
    {!! Form::text('name', null,['class' => 'form-control','id' => 'name','placeholder' => 'Enter name']); !!}
    @include('includes.single_field_validation',['field'=>'name'])
</div>

<div class="form-group">
    {!!  Form::label('slug', 'Slug'); !!}
    {!! Form::text('slug', null,['class' => 'form-control','id' => 'slug']); !!}

    @include('includes.single_field_validation',['field'=>'slug'])
</div>

<div class="form-group">
    {!!  Form::label('price', 'Price'); !!}
    {!! Form::number('price', null,['class' => 'form-control','id' => 'price','step' => '0.001']); !!}

    @include('includes.single_field_validation',['field'=>'price'])
</div>

<div class="form-group">
    {!!  Form::label('quantity', 'Quantity'); !!}
    {!! Form::number('quantity', null,['class' => 'form-control','id' => 'quantity']); !!}

    @include('includes.single_field_validation',['field'=>'quantity'])
</div>

<div class="form-group">
    {!!  Form::label('stock', 'Stock'); !!}
    {!! Form::number('stock', null,['class' => 'form-control','id' => 'stock']); !!}

    @include('includes.single_field_validation',['field'=>'stock'])
</div>

<div class="form-group">
    {!!  Form::label('photo', 'Image'); !!}
    {!! Form::file('photo', null,['class' => 'form-control','id' => 'photo']); !!}

    @include('includes.single_field_validation',['field'=>'photo'])

    @if(isset($data['product']) && $data['product']->image)
        <img id="image" src="{{asset('images/product/' . $data['product']->image)}}" width="100" height="100">
        {!! Form::hidden('photo', $data['product']->image); !!}
    @endif
</div>

<div class="form-group">
    {!!  Form::label('short_description', 'Short Description'); !!}
    {!! Form::textarea('short_description', null,['class' => 'form-control','id' => 'short_description']); !!}

    @include('includes.single_field_validation',['field'=>'short_description'])
</div>

<div class="form-group">
    {!!  Form::label('description', 'Description'); !!}
    {!! Form::textarea('description', null,['class' => 'form-control','id' => 'description']); !!}

    @include('includes.single_field_validation',['field'=>'description'])
</div>

<div class="form-group">
    {!!  Form::label('feature_key', 'Feature Key'); !!}
    {!! Form::radio('feature_key', '1') !!} Active
    {!! Form::radio('feature_key', '0',true) !!} De Active
</div>

<div class="form-group">
    {!!  Form::label('discount_key', 'Discount Key'); !!}
    {!! Form::radio('discount_key', '1') !!} Active
    {!! Form::radio('discount_key', '0',true) !!} De Active
</div>

<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>
