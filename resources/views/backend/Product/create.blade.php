@extends('home')
@section('title','Product Create page')
@section('js')
    <script>
        $("#name").keyup(function(){
            var Text = $(this).val();
            Text = Text.toLowerCase();
            Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
            $("#slug").val(Text);
        });
    </script>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Product Management
            <a href="{{route('product.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="{{route('home')}}">Home</a></li>
            <li style="padding-right: 10px"><a href="">Category</a></li>
            <li>Create page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                {!! Form::open(['route' => 'product.store', 'method' => 'post','files' => true]) !!}
                @include('backend.product.mainform')

                <div class="form-group">

                    <button type="submit" class="btn btn-success"   value="Save Category"><i class="fa fa-save"></i>Save Category</button>
                    <button type="submit" class="btn btn-danger"   value="Clear"><i class="fa fa-recycle"></i>Cancel</button>
                </div>
                {!! Form::close() !!}

            </div>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
