@extends('home')
@section('title','Product View page')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Product Management
            <a href="{{route('product.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
            <a href="{{route('product.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="#">Home</a></li>
            <li style="padding-right: 10px"><a href="#">Category</a></li>
            <li>Create page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">

            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Category Name</th>
                        <td>{{$data['product']->category->name}}</td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <td>{{$data['product']->name}}</td>
                    </tr>
                    <tr>
                        <th>Slug</th>
                        <td>{{$data['product']->slug}}</td>
                    </tr>
                    <tr>
                        <th>Price</th>
                        <td>{{$data['product']->price}}</td>
                    </tr>
                    <tr>
                        <th>Quantity</th>
                        <td>{{$data['product']->quantity}}</td>
                    </tr>
                    <tr>
                        <th>Stock</th>
                        <td>{{$data['product']->stock}}</td>
                    </tr>

                    <tr>
                        <th>Image</th>
                        <td>
                            <div class="img-container">
                                <a href="{{asset('images/product/' .$data['product']->image)}}"data-title="{{$data['product']->image}}">
                                    <img src="{{asset('images/product/' .$data['product']->image)}}" alt="" height="100" width="100">
                                </a>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th>Short Description</th>
                        <td>{{$data['product']->short_description}}</td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td>{{$data['product']->description}}</td>
                    </tr>

                    <tr>
                        <th>Status</th>
                        <td>
                            @if($data['product']->status==1)
                                <span> Active </span>
                            @else
                                <span> Inactive </span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Created By</th>
                        <td>{{\Illuminate\Foundation\Auth\User::find($data['product']->created_by)->name}}</td>
                    </tr>

                    <tr>
                        <th>Created At</th>
                        <td>{{$data['product']->created_at->format('j F,Y')}}</td>
                    </tr>
                    <tr>
                        <th>Updated At</th>
                        <td>{{$data['product']->updated_at->format('j F,Y')}}</td>
                    </tr>
                    <tr>
                        <th>Deleted At</th>
                        <td>
                            {{--                            {{$data['product']->deleted_at->format('j F,Y') ?? ''}}--}}
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
