@extends('home')
@section('title', 'Module Create Page')

@section('content')
    <section class="content-header">
        <h1>
            Module Management
            <a href="{{route('module.index')}}" class="btn btn-info">
            <i class="fa fa-list"></i>List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="{{route('home')}}">Home</a></li>
            <li style="padding-right: 10px"><a href="{{route('module.index')}}"> Module</a></li>
            <li>Create Page</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                {!! Form::open(['route'=> 'module.store', 'method' => 'post']) !!}
                @include('backend.module.mainform')

                <div class="form-group">
                    <button type="submit" class="btn btn-success" value="Save Module"><i class="fa fa-save"></i>Save Module</button>
                    <button type="reset" class="btn btn-danger" value="Clear"><i class="fa fa-recycle"></i>Cancel</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>

    </section>
@endsection

