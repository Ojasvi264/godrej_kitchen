@extends('home')
@section('title', 'Module Index Page')

@section('content')
    <section class="content-header">
        <h1>Module Management
            <a href="{{route('module.create')}}" class="btn btn-success"><i class="fa fa-plus"></i>Create</a>
        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="{{route('home')}}">Home</a></li>
            <li style="padding-right: 10px"><a href="{{route('module.index')}}">Module</a></li>
            <li>Index Page</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Name</th>
                        <th>Route</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($data['modules'] as $module)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$module->name}}</td>
                            <td>{{$module->route}}</td>
                            <td>
                                @if($module->status == 1)
                                    <span style="color: #1cc88a ">Active</span>
                                @else
                                    <span style="color: red">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('module.show',$module->id)}}" class="btn btn-info"> <i class="fa fa-eye"></i>View</a>
                                <a  href="{{route('module.edit',$module->id)}}" class="btn btn-warning"> <i class="fa fa-pencil"></i>Edit</a>

                                <form style="display: inline-block" action="{{route('module.destroy',$module->id)}}" method="post" onsubmit="return confirm('Are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box -->

    </section>
@endsection
