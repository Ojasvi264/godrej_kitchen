@extends('home')
@section('title','Module View page')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Module Management
            <a href="{{route('module.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
            <a href="{{route('module.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="#">Home</a></li>
            <li style="padding-right: 10px"><a href="#">Module</a></li>
            <li>Create page</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <td>{{$data['module']->name}}</td>
                    </tr>
                    <tr>
                        <th>Route</th>
                        <td>{{$data['module']->route}}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>
                            @if($data['module']->status==1)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger">Inactive</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Created By</th>
                        <td>{{\Illuminate\Foundation\Auth\User::find($data['module']->created_by)->name}}</td>
                    </tr>

                    <tr>
                        <th>Created At</th>
                        <td>{{$data['module']->created_at->format('j F,Y')}}</td>
                    </tr>
                    <tr>
                        <th>Updated At</th>
                        <td>{{$data['module']->updated_at->format('j F,Y')}}</td>
                    </tr>
                    <tr>
                        <th>Deleted At</th>
                        <td>
                            {{--                            {{$data['module']->deleted_at->format('j F,Y') ?? ''}}--}}
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

