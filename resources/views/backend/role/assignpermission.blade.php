@extends('home')
@section('title','Role assignpermission page')

@section('content')
    <section class="content-header">
        <h1>
            Role Management
            <a href="{{route('role.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
            <a href="{{route('role.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="{{route('home')}}"> Home</a></li>
            <li style="padding-right: 10px"><a href="{{route('role.index')}}">Role</a></li>
            <li class="active">Assign Permission Role</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="{{route('role.savepermission',$data['role']->id)}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="name">Role Name:</label>
                        {{$data['role']->name}}
                    </div>

                    <div class="form-group">
                       <ul type="none">
                           @foreach($data['modules'] as $module)
                               <li>{{$module->name}}</li>
                               <ul type="none">
                                   @foreach($module->permissions as $permission)
                                       <li>
                                           <input type="checkbox" name="permission_id[]" value="{{$permission->id}}"
                                                  @if(in_array($permission->id, $data['permissions'])) checked="checked"
                                           @endif  > {{$permission->name}}
                                       </li>
                                   @endforeach
                               </ul>
                           @endforeach
                       </ul>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success" value="Save Role"><i class="fa fa-save"></i>Save Permission</button>
                        <button type="reset" class="btn btn-danger" value="Clear"><i class="fa fa-recycle"></i>Cancel</button>
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
