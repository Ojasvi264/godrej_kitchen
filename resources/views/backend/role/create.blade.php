@extends('home')
@section('title','Role Create page')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Role Management
            <a href="{{route('role.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="{{route('home')}}">Home</a></li>
            <li style="padding-right: 10px"><a href="{{route('role.index')}}">Role</a></li>
            <li class="active">Create Role</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                {!! Form::open(['route' => 'role.store', 'method' => 'post']) !!}
                @include('backend.role.mainform')


                <div class="form-group">

                    <button type="submit" class="btn btn-success"   value="Save Role"><i class="fa fa-save"></i>Save Role</button>
                    <button type="reset" class="btn btn-danger"   value="Clear"><i class="fa fa-recycle"></i>Cancel</button>
                </div>
                {!! Form::close() !!}

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
