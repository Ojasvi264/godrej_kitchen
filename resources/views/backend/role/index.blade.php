@extends('home')
@section('title','Role index page')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Role Management
            <a href="{{route('role.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="{{route('home')}}"> Home</a></li>
            <li style="padding-right: 10px"><a href="{{route('role.create')}}">Role</a></li>
            <li class="active">Index Role</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body">
               @include('includes.flash')
                <table class="table table-bordered" id="datatable">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($data['roles'] as $role)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$role->name}}</td>
                            <td>
                                @if($role->status == 1)
                                    <span style="color: #1cc88a ">Active</span>
                                @else
                                    <span style="color: red">Inactive</span>
                                @endif
                            </td>

                            <td>
                                <a href="{{route('role.show',$role->id)}}" class="btn btn-info">
                                    <i class="fa fa-eye"></i>
                                    View
                                </a>

                                <a href="{{route('role.edit',$role->id)}}" class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>

                                <a href="{{route('role.assignpermission',$role->id)}}" class="btn btn-success">
                                    <i class="fa fa-eye"></i>
                                    Assign Permission
                                </a>
                                <form style="display: inline-block" action="{{route('role.destroy',$role->id)}}" method="post"
                                      onsubmit="return confirm('Are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                </form>                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>

    </section>
@endsection
