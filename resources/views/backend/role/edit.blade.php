@extends('home')
@section('title','Role edit page')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Role Management
            <a href="{{route('role.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
            <a href="{{route('role.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="{{route('home')}}">Home</a></li>
            <li style="padding-right: 10px"><a href="{{route('role.index')}}">Role</a></li>
            <li class="active">Edit page</li>
        </ol>
    </section>

    <!-- Main content -->

    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            @include('includes.flash')
            @include('includes.error')
            {!! Form::model($data['role'], ['route' => ['role.update', $data['role']->id],'method' => 'put']) !!}
            @include('backend.role.mainform')
            <div class="form-group">
                {{ Form::button('<i class="fa fa-save"></i> Update Role', ['type' => 'submit', 'class' => 'btn btn-warning btn-sm'] )  }}
                <button type="reset" class="btn btn-danger"   value="Clear"><i class="fa fa-recycle"></i>Cancel</button>
            </div>
            {!! Form::close() !!}

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
