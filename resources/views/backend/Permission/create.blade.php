@extends('home')
@section('title', 'Permission Create Page')

@section('content')
    <section class="content-header">
        <h1>
            Permission Management
            <a href="{{route('permission.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="{{route('home')}}">Home</a></li>
            <li style="padding-right: 10px"><a href="{{route('permission.index')}}"> Permission</a></li>
            <li>Create Page</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                {!! Form::open(['route'=> 'permission.store', 'method' => 'post']) !!}
                @include('backend.permission.mainform')

                <div class="form-group">
                    <button type="submit" class="btn btn-success" value="Save Permission"><i class="fa fa-save"></i>Save Permission</button>
                    <button type="reset" class="btn btn-danger" value="Clear"><i class="fa fa-recycle"></i>Cancel</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>

    </section>
@endsection

