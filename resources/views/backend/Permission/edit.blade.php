@extends('home')
@section('title','Permission Edit page')
@section('content')
    <section class="content-header">
        <h1>
            Permission Management
            <a href="{{route('permission.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>

            <a href="{{route('permission.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="#">Home</a></li>
            <li style="padding-right: 10px"><a href="#">Permission</a></li>
            <li>Edit page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                @include('includes.flash')
                @include('includes.error')
                {!! Form::model($data['permission'], ['route' => ['permission.update', $data['permission']->id],'method' => 'put']) !!}
                @include('backend.permission.mainform')
                <div class="form-group">
                    {{ Form::button('<i class="fa fa-save"></i> Update Permission', ['type' => 'submit', 'class' => 'btn btn-warning'] )  }}
                    <button type="submit" class="btn btn-danger"   value="Clear"><i class="fa fa-recycle"></i>Cancel</button>
                </div>
                {!! Form::close() !!}

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
