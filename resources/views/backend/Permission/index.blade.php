@extends('home')
@section('title', 'Permission Index Page')

@section('content')
    <section class="content-header">
        <h1>Permission Management
            <a href="{{route('permission.create')}}" class="btn btn-success"><i class="fa fa-plus"></i>Create</a>
        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="{{route('home')}}">Home</a></li>
            <li style="padding-right: 10px"><a href="{{route('permission.index')}}">Permission</a></li>
            <li>Index Page</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Module Name</th>
                        <th>Name</th>
                        <th>Route</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($data['permissions'] as $permission)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$permission->module->name}}</td>
                            <td>{{$permission->name}}</td>
                            <td>{{$permission->route}}</td>
                            <td>
                                @if($permission->status == 1)
                                    <span style="color: #1cc88a ">Active</span>
                                @else
                                    <span style="color: red">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('permission.show',$permission->id)}}" class="btn btn-info"> <i class="fa fa-eye"></i>View</a>
                                <a  href="{{route('permission.edit',$permission->id)}}" class="btn btn-warning"> <i class="fa fa-pencil"></i>Edit</a>

                                <form style="display: inline-block" action="{{route('permission.destroy',$permission->id)}}" method="post" onsubmit="return confirm('Are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box -->

    </section>
@endsection
