@extends('home')
@section('title','Category View page')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Category Management
            <a href="{{route('category.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
            <a href="{{route('category.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="#">Home</a></li>
            <li style="padding-right: 10px"><a href="#">Category</a></li>
            <li>Create page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">

            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <td>{{$data['category']->name}}</td>
                    </tr>
                    <tr>
                        <th>Rank</th>
                        <td>{{$data['category']->rank}}</td>
                    </tr>
                    <tr>
                        <th>Slug</th>
                        <td>{{$data['category']->slug}}</td>
                    </tr>
                    <tr>
                        <th>Meta Keyword</th>
                        <td>{{$data['category']->meta_keyword}}</td>
                    </tr>
                    <tr>
                        <th>Meta Description</th>
                        <td>{{$data['category']->meta_description}}</td>
                    </tr>

                    <tr>
                        <th>Status</th>
                        <td>
                            @if($data['category']->status==1)
                                <span> Active </span>
                            @else
                                <span> Inactive </span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Created By</th>
                        <td>{{\Illuminate\Foundation\Auth\User::find($data['category']->created_by)->name}}</td>
                    </tr>

                    <tr>
                        <th>Created At</th>
                        <td>{{$data['category']->created_at->format('j F,Y')}}</td>
                    </tr>
                    <tr>
                        <th>Updated At</th>
                        <td>{{$data['category']->updated_at->format('j F,Y')}}</td>
                    </tr>
                    <tr>
                        <th>Deleted At</th>
                        <td>
{{--                            {{$data['category']->deleted_at->format('j F,Y') ?? ''}}--}}
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
