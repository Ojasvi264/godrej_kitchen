@extends('home')
@section('title', 'Category index page')

@section('content')
    <section class="content-header">
        <h1>
            Category Management
            <a href="{{route('category.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
        </h1>
        <ol class="breadcrumb">
            <li style="padding-right: 10px"><a href="{{route('home')}}"> Home</a></li>
            <li style="padding-right: 10px"><a href="{{route('category.index')}}">Category</a></li>
            <li>Index page</li>
        </ol>
    </section>
    <section class="content">

        <!-- Default box -->
        <div class="box">

            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Name</th>
                        <th>Rank</th>
                        <th>Slug</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php($i=1)
                        @foreach($data['categories'] as $category)
                           <tr>
                               <td>{{$i++}}</td>
                               <td>{{$category->name}}</td>
                               <td>{{$category->rank}}</td>
                               <td>{{$category->slug}}</td>
{{--                               <td>{{$category->meta_keyword}}</td>--}}
{{--                               <td>{{$category->meta_description}}</td>--}}
                               <td>
                                   @if($category->status == 1)
                                       <span style="color: #1cc88a ">Active</span>
                                   @else
                                       <span style="color: red">Inactive</span>
                                   @endif
                               </td>
                               <td>
                                   <a href="{{route('category.show',$category->id)}}" class="btn btn-info"> <i class="fa fa-eye"></i>View</a>
                                   <a  href="{{route('category.edit',$category->id)}}" class="btn btn-warning"> <i class="fa fa-pencil"></i>Edit</a>

                                   <form style="display: inline-block" action="{{route('category.destroy',$category->id)}}" method="post" onsubmit="return confirm('Are you sure?')">
                                       @csrf
                                       <input type="hidden" name="_method" value="DELETE"/>
                                       <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                   </form>
                               </td>
                           </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box -->

    </section>
@endsection


